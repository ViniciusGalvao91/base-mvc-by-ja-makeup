<?php

namespace APP\Models;

use SON\Db\Table;

class Clientes extends Table{
    
    //aqui eu indico qual a tabela que eu quero consultar. Ex: se eu quiser a tabela usuario é só criar a classe extends table e na variavel table eu colocar "usuario"
    protected $table = "clientes";   
    
    private $id_cliente;
    private $nome;
    private $celular;
    private $telefone;
    private $cpf;
    private $cep;
    
  public function __construct($id_cliente, $nome, $celular, $telefone, $cpf, $cep){
        
      $this->id_cliente = $id_cliente;     
      $this->nome = $nome;
      $this->celular = $celular;
      $this->telefone = $telefone;
      $this->cpf = $cpf;
      $this->cep = $cep;      
    }
    
    public function setId_cliente($id_cliente){
        
        $this->id_cliente = $id_cliente;
    }
    
     public function getId_cliente(){
        
        $this->id_cliente = $id_cliente;
         
         return $this->id_cliente;
    }
    
    public function setNome($nome){
        
        $this->nome = $nome;
    }
    
     public function getNome(){
        
        $this->nome = $nome;
         
         return $this->nome;
    }
    public function setCelular($celular){
        
        $this->celular = $celular;
    }
    
     public function getCelular(){
        
        $this->celular = $celular;
         
         return $this->celular;
    }
    public function setTelefone($telefone){
        
        $this->telefone = $telefone;
    }
    
     public function getTelefone(){
        
        $this->telefone = $telefone;
         
         return $this->telefone;
    }
    public function setCpf($cpf){
        
        $this->cpf = $cpf;
    }
    
     public function getCpf(){
        
        $this->cpf = $cpf;
         
         return $this->cpf;
    }
    public function setCep($cep){
        
        $this->cep = $cep;
    }
    
     public function getCep(){
        
        $this->cep = $cep;
         
         return $this->cep;
    }
   
    
      
    
}