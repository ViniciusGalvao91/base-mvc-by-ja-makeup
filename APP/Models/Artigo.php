<?php

namespace APP\Models;

use SON\Db\Table;

class Artigo extends Table{
    
    //aqui eu indico qual a tabela que eu quero consultar. Ex: se eu quiser a tabela usuario é só criar a classe extends table e na variavel table eu colocar "usuario"
    protected $table = "artigos";   
    
}