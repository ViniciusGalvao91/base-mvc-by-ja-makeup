<?php

namespace APP\Models;

use SON\Db\Table;

class Servicos extends Table{
    
    //aqui eu indico qual a tabela que eu quero consultar. Ex: se eu quiser a tabela usuario é só criar a classe extends table e na variavel table eu colocar "usuario"
    protected $table = "servicos";   
    
    private $id_servico;
    private $nome;
    private $descricao;
    private $preco;
  
    
  public function __construct($id_servico, $nome, $descricao, $preco){
        
      $this->id_servico = $id_servico;     
      $this->nome = $nome;
      $this->descricao = $descricao;
      $this->preco = $preco;
  
    }
    
    public function setId_servico($id_servico){
        
        $this->id_servico = $id_servico;
    }
    
     public function getId_servico(){
        
        $this->id_servico = $id_servico;
         
         return $this->id_servico;
    }
    
    public function setNome($nome){
        
        $this->nome = $nome;
    }
    
     public function getNome(){
        
        $this->nome = $nome;
         
         return $this->nome;
    }
    public function setDescricao($descricao){
        
        $this->descricao = $descricao;
    }
    
     public function getDescricao(){
        
        $this->descricao = $descricao;
         
         return $this->descricao;
    }
    public function setPreco($preco){
        
        $this->preco = $preco;
    }
    
     public function getPreco(){
        
        $this->preco = $preco;
         
         return $this->preco;
    } 
}