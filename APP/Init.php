<?php

namespace APP;

use SON\Init\Bootstrap;

class Init extends Bootstrap{
    
    /**aqui colocamos as rotas que podem ser acessadas / encaminhamento para os controllers models ou view
    */ 
    protected function initRoutes(){
        
        $ar['home'] = array('route'=>'/','controller'=>'index','action'=>'index');
        $ar['orcamentos'] = array('route'=>'/orcamentos','controller'=>'index','action'=>'orcamentos');
        $ar['clientes'] = array('route'=>'/clientes','controller'=>'index','action'=>'clientes');
        $ar['atendimentos'] = array('route'=>'/atendimentos','controller'=>'index','action'=>'atendimentos');
        $ar['servicos'] = array('route'=>'/servicos','controller'=>'index','action'=>'servicos');
        $this->setRoutes($ar);
    }
    
    public static function getDb(){
        
        try{
            
            $db = new \PDO("mysql:host=localhost;dbname=mvc","root","root");
            return $db;
        }catch (Exception $e){
            
            echo"Falha na conexão com a base!!\n";
        }  
    }
}