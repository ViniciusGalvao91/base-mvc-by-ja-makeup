<?php

namespace APP\Controllers;

use SON\Controller\Action;
use \SON\Di\Container;

class Index extends Action{
        
    //envia para a tela o conteudo retornado da classe init
    public function index(){
    
        //instancia a classe "Artigo" do model com a conexao com o db
        $artigo = Container::getClass("Artigo");
        //instancia os metodos da classe "Table" que tem os metodos para o banco
        $artigos = $artigo->fetAll();        
        
        //envia os dados para a view
        $this->view->artigos = $artigos;
                        
        //action que desejo renderizar
        $this->render('index');
    }
    
    //envia para a tela o conteudo retornado da classe init
    public function orcamentos(){
       
        //renderizando
        $this->render('orcamentos');
    }
    
    public function clientes(){
       
        //renderizando
        $this->render('clientes');
    }
    
      public function atendimentos(){
       
        //renderizando
        $this->render('atendimentos');
    }
    
      public function servicos(){
       
        //renderizando
        $this->render('servicos');
    }
}

