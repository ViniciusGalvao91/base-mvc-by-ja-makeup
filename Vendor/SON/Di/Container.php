<?php

namespace SON\Di;

class Container{
    
    public static function getClass($name){
        
        $str_class = "\\APP\\Models\\".ucfirst($name);
        $class = new $str_class(\APP\Init::getDb());
        return $class;
    }
    
}